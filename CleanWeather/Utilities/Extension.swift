//
//  Extension.swift
//  CleanWheather
//
//  Created by Marco Del Giudice on 15/04/2020.
//  Copyright © 2020 Marco Del Giudice. All rights reserved.
//

import Foundation

import Foundation
import UIKit


var imageCache = NSCache<AnyObject, AnyObject>()
private let queue = OperationQueue()

func mainThread(_ block: @escaping () -> ()) {
    if !Thread.isMainThread {
        DispatchQueue.main.async  {
            block()
        }
    } else {
        block()
    }
}

extension UIImageView {
    
    func loadImage(urlString: String) {
        
        if let cacheImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = cacheImage
            return
        }
        
        guard let url = URL(string: urlString) else { return }
        let op = NetworkImageOperation(url: url)
        op.completionBlock = {
            guard let img = op.image else {return}
            imageCache.setObject(img, forKey: urlString as AnyObject)
            mainThread {
                self.image = img
            }
        }
        queue.addOperation(op)
        
    }
    
    
}


