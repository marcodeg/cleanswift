//
//  BaseForecast.swift
//  CleanWheather
//
//  Created by Marco Del Giudice on 14/04/2020.
//  Copyright © 2020 Marco Del Giudice. All rights reserved.
//

import Foundation
struct BaseForecast : Codable {
    let lat : Double?
    let lon : Double?
    let timezone : String?
    let daily : [Daily]?

    enum CodingKeys: String, CodingKey {

        case lat = "lat"
        case lon = "lon"
        case timezone = "timezone"
        case daily = "daily"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        lat = try values.decodeIfPresent(Double.self, forKey: .lat)
        lon = try values.decodeIfPresent(Double.self, forKey: .lon)
        timezone = try values.decodeIfPresent(String.self, forKey: .timezone)
        daily = try values.decodeIfPresent([Daily].self, forKey: .daily)

    }

}
