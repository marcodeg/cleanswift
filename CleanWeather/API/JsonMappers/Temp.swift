//
//  Temp.swift
//  CleanWheather
//
//  Created by Marco Del Giudice on 14/04/2020.
//  Copyright © 2020 Marco Del Giudice. All rights reserved.
//

import Foundation
struct Temp : Codable {
    let day : Double?
    let min : Double?
    let max : Double?
    let night : Double?
    let eve : Double?
    let morn : Double?

    enum CodingKeys: String, CodingKey {

        case day = "day"
        case min = "min"
        case max = "max"
        case night = "night"
        case eve = "eve"
        case morn = "morn"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        day = try values.decodeIfPresent(Double.self, forKey: .day)
        min = try values.decodeIfPresent(Double.self, forKey: .min)
        max = try values.decodeIfPresent(Double.self, forKey: .max)
        night = try values.decodeIfPresent(Double.self, forKey: .night)
        eve = try values.decodeIfPresent(Double.self, forKey: .eve)
        morn = try values.decodeIfPresent(Double.self, forKey: .morn)
    }

}
