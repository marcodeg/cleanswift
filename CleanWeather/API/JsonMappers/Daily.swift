//
//  Daily.swift
//  CleanWheather
//
//  Created by Marco Del Giudice on 14/04/2020.
//  Copyright © 2020 Marco Del Giudice. All rights reserved.
//

import Foundation
struct Daily : Codable {
    let dt : Int?
    let sunrise : Int?
    let sunset : Int?
    let temp : Temp?
    let feels_like : Feels_like?
    let pressure : Int?
    let humidity : Int?
    let dew_point : Double?
    let wind_speed : Double?
    let wind_deg : Int?
    let weather : [Weather]?
    let clouds : Int?
    let uvi : Double?

    enum CodingKeys: String, CodingKey {

        case dt = "dt"
        case sunrise = "sunrise"
        case sunset = "sunset"
        case temp = "temp"
        case feels_like = "feels_like"
        case pressure = "pressure"
        case humidity = "humidity"
        case dew_point = "dew_point"
        case wind_speed = "wind_speed"
        case wind_deg = "wind_deg"
        case weather = "weather"
        case clouds = "clouds"
        case uvi = "uvi"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dt = try values.decodeIfPresent(Int.self, forKey: .dt)
        sunrise = try values.decodeIfPresent(Int.self, forKey: .sunrise)
        sunset = try values.decodeIfPresent(Int.self, forKey: .sunset)
        temp = try values.decodeIfPresent(Temp.self, forKey: .temp)
        feels_like = try values.decodeIfPresent(Feels_like.self, forKey: .feels_like)
        pressure = try values.decodeIfPresent(Int.self, forKey: .pressure)
        humidity = try values.decodeIfPresent(Int.self, forKey: .humidity)
        dew_point = try values.decodeIfPresent(Double.self, forKey: .dew_point)
        wind_speed = try values.decodeIfPresent(Double.self, forKey: .wind_speed)
        wind_deg = try values.decodeIfPresent(Int.self, forKey: .wind_deg)
        weather = try values.decodeIfPresent([Weather].self, forKey: .weather)
        clouds = try values.decodeIfPresent(Int.self, forKey: .clouds)
        uvi = try values.decodeIfPresent(Double.self, forKey: .uvi)
    }
    
    init() {
        self.dt = nil
        self.sunrise = nil
        self.sunset = nil
        self.temp = nil
        self.feels_like = nil
        self.pressure = nil
        self.humidity = nil
        self.dew_point = nil
        self.wind_speed = nil
        self.wind_deg = nil
        self.weather = nil
        self.clouds = nil
        self.uvi = nil
    }

}
